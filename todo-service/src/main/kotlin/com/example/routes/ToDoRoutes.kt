package com.example.routes

import com.example.models.ToDoDTO
import com.example.plugins.JwtCheck
import com.example.services.ToDoService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.todoRoutes(){

    val toDoService: ToDoService = ToDoService()

    route("/todo"){
        post("/all"){
            val req = call.receive<ToDoDTO>()
            val token = req.token
            val res = toDoService.getAllToDos(token)
            if (res["error"] == "false"){
                call.respond(HttpStatusCode.OK, res)
            }
            else{
                call.respond(HttpStatusCode.Unauthorized, res)
            }
        }
        post("/{id}"){
            val req = call.receive<ToDoDTO>()
            val token = req.token
            val id : String? = call.parameters["id"].toString()
            val res = toDoService.getToDoById(id, token)
            if(res["error"] == "false"){
                call.respond(HttpStatusCode.OK,res)
            }
            else{
                call.respond(HttpStatusCode.Unauthorized, res)
            }
        }


        post("/add"){
            val req = call.receive<ToDoDTO>()
            val res = toDoService.addNewToDo(req)
            if (res["error"] == "false"){
                call.respond(HttpStatusCode.Created, res)
            }
            else{
                call.respond(HttpStatusCode.Unauthorized, res)
            }
        }
        put("/{id}"){
            val req = call.receive<ToDoDTO>()
            val id : String? = call.parameters["id"].toString()
            val res = toDoService.updateToDo(id, req)
            if(res["error"] == "false"){
                call.respond(HttpStatusCode.OK, res)
            }
            else{
                call.respond(HttpStatusCode.Unauthorized, res)
            }
        }

        delete("/{id}"){
            val req = call.receive<ToDoDTO>()
            val id  : String? = call.parameters["id"].toString()
        }

    }
}