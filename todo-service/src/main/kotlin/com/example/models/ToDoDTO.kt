package com.example.models

data class ToDoDTO(
    val title: String,
    val status: Boolean,
    val token: String,
)
