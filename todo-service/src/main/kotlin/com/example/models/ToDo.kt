package com.example.models

import org.bson.codecs.pojo.annotations.BsonId

data class ToDo(
    @BsonId
    val id: String? = null,
    val title: String,
    val status: Boolean,
    val userId: String,
)
