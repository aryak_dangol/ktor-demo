package com.example

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import io.ktor.server.application.*

fun main() {
    embeddedServer(Netty, port = 5000, host = "0.0.0.0") {
        configureMonitoring()
        configureHTTP()
        configureSerialization()
        configureRouting()
    }.start(wait = true)
}
