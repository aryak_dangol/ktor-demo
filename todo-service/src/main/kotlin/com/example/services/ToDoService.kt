package com.example.services

import com.example.models.ToDo
import com.example.models.ToDoDTO
import com.example.repositories.ToDoRepository
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import org.litote.kmongo.util.idValue
import java.net.http.HttpHeaders
import java.net.http.HttpResponse

class ToDoService {

    private val URL = "http://localhost:3501/user/get-user"

    private val toDoRepository: ToDoRepository = ToDoRepository()

    private suspend fun getUserId(token: String): String?{
        val client = HttpClient(CIO){
            install(ContentNegotiation) {
                gson()
            }
        }
        val response = client.get(URL){
            headers{
                append("dapr-app-id", "user-service")
                append("Authorization", "Bearer $token")
            }
        }
        return if (response.status == HttpStatusCode.OK){
            val map : HashMap<String, String> = response.body()
            map["id"]
        } else{
            null
        }


    }

    suspend fun getAllToDos(token: String): HashMap<String, Any>{
        val map : HashMap<String, Any> = HashMap()
        val userid : String? = getUserId(token)
        if(userid != null){
            val todosForUser = toDoRepository.getAllToDo(userid)
            return if (todosForUser.isNotEmpty()){
                map["data"] = todosForUser
                map["error"] = "false"
                map
            } else{
                map["data"] = listOf(null)
                map["message"] = "No ToDo Found For User"
                map["error"] = "false"
                map["userId"] = userid
                map
            }
        }
        else{
            map["error"] = "true"
            map["message"] = "Authentication Failed!"
            return map
        }
    }

    suspend fun getToDoById(id: String?, token: String): HashMap<String, Any>{
        val map : HashMap<String, Any> = HashMap()
        val userid : String? = getUserId(token)
        if(userid != null && id!= null){
            val todo: ToDo? = toDoRepository.getToDoById(id)
                return if (todo != null) {
                    map["data"] = todo
                    map["error"] = "false"
                    map
                } else {
                    map["data"] = ""
                    map["error"] = "false"
                    map["message"] = "No Data found for given $id"
                    map
                }
            }
        else{
            map["error"] = "true"
            map["message"] = "Authentication Failed!"
            return map
        }
    }


    suspend fun addNewToDo(todo: ToDoDTO): HashMap<String, Any> {
        val map : HashMap<String, Any> = HashMap()
        val userid : String? = getUserId(todo.token)
        return if(userid != null){
            val newToDo = toDoRepository.addToDo(ToDo(null, todo.title, todo.status, userid))
            map["data"] = newToDo
            map["error"] = "false"
            map
        } else{
            map["error"] = "true"
            map["message"] = "Authentication Failed!"
            map
        }
    }

    suspend fun updateToDo(id: String?, todo: ToDoDTO): HashMap<String, Any>{
        val map : HashMap<String, Any> = HashMap()
        val userid : String? = getUserId(todo.token)
        if(userid!=null && id != null){
            val updatedToDo : ToDo? = toDoRepository.updateToDo(ToDo(id, todo.title, todo.status, userid))
            return if(updatedToDo != null){
                map["data"] = updatedToDo
                map["error"] = false
                map["message"] = "To Do with ${updatedToDo.id} is updated"
                map
            } else{
                map["data"] = ""
                map["error"] = false
                map["message"] = "No To Do with $id found!"
                map
            }

        }
        else{
            map["error"] = "true"
            map["message"] = "Authentication Failed!"
            return map
        }
    }

    suspend fun deleteToDo(id: String?, token: String): HashMap<String, Any>{
        val map : HashMap<String, Any> = HashMap()
        val userid : String? = getUserId(token)
        if(userid != null && id != null){
            val deletedToDo : ToDo? = toDoRepository.deleteToDo(id)
            return if (deletedToDo != null){
                map["data"] = deletedToDo
                map["message"] = "Successfully Deleted"
                map["error"] = "false"
                map
            } else{
                map["data"] = ""
                map["error"] = "true"
                map["message"] = "No To Do Found for $id to delete"
                map
            }
        }
        else{
            map["error"] = "true"
            map["message"] = "Authentication Failed!"
            return map
        }

    }



}