package com.example.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*

val JwtCheck = createRouteScopedPlugin(name = "JwtCheck"){
    onCall { call ->
        val token: String? = call.request.headers["Authorization"].toString()
        if(token == null){
            call.respond(HttpStatusCode.Unauthorized, mapOf("error" to true, "message" to "Authentication Failed"))
        }
    }
}