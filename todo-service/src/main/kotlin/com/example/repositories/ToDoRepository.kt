package com.example.repositories

import com.example.models.ToDo
import com.example.models.ToDoDTO
import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReturnDocument
import org.litote.kmongo.*

class ToDoRepository {
    private val client = KMongo.createClient( "mongodb+srv://aryak:aryak123@cluster0.tewmglw.mongodb.net/?retryWrites=true&w=majority")
    private val db = client.getDatabase("ktor-demo")
    private val todoColection = db.getCollection<ToDo>()

    fun getAllToDo(userid: String): List<ToDo> {
        return todoColection.find(ToDo::userId eq userid).toList()
    }

    fun addToDo(todo: ToDo): ToDo{
        todoColection.insertOne(todo)
        return todo
    }

    fun getToDoById(id: String): ToDo? {
        return todoColection.findOne(ToDo::id eq id)
    }

    fun deleteToDo(id: String): ToDo? {
        return todoColection.findOneAndDelete(ToDo::id eq id)
    }

    fun updateToDo(todo: ToDo): ToDo? {
        val options = FindOneAndReplaceOptions()
        options.returnDocument(ReturnDocument.AFTER)
        return todoColection.findOneAndReplace(
            ToDo::id eq todo.id,
            todo,
            options
        )
    }

}