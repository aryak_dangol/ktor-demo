package com.example.routes

import com.example.models.ToDo
import com.example.models.ToDoDTO
import com.example.plugins.configureRouting
import com.example.plugins.configureSerialization
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import io.ktor.server.testing.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


internal class ToDoRoutesKtTest {

    val token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyLXNlcnZpY2UiLCJpc3MiOiJ1c2VyLXNlcnZpY2UiLCJpZCI6IjYyZjI4NjM2MjEzN2I3MTdmYmE2MGY5ZSJ9.XOIyjMdnLAtGQRa0_LphB07VvLn3KYxPxpe5D9DPIK4"

    @Test
    fun testGetAllToDos() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
               gson()
            }
        }

        val res = client.post("/todo/all"){
            contentType(ContentType.Application.Json)
            setBody(mapOf("token" to token))
        }
       val hm : HashMap<String, Any> = res.body()
        println(hm["data"])
        assertNotNull(hm["data"])
        assertEquals("false", hm["error"])
    }

    @Test
    fun testGetToDoById() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
                gson()
            }
        }

        val res = client.post("/todo/62f2d4a601b6c24b29efee25"){
            contentType(ContentType.Application.Json)
            setBody(mapOf("token" to token))
        }
        val hm : HashMap<String, Any> = res.body()
        val result: Map<String, String> = hm["data"] as Map<String, String>
        assertNotNull(hm["data"])
        assertEquals("Visit Vet", result["title"])
    }

    @Test
    fun testAddToDo() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
                gson()
            }
        }

        val res = client.post("/todo/add"){
            contentType(ContentType.Application.Json)
            setBody(ToDoDTO("Go to Supermarket", false, token))
        }
        val hm : HashMap<String, Any> = res.body()
        val result: Map<String, String> = hm["data"] as Map<String, String>
        assertNotNull(hm["data"])
        assertEquals("Go to Supermarket", result["title"])
    }

    @Test
    fun testUpdateToDo() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
                gson()
            }
        }

        val updatedTitle = "Visit Vet and Go to Gym at 3"

        val res = client.put("/todo/62f2d4a601b6c24b29efee25"){
            contentType(ContentType.Application.Json)
            setBody(ToDoDTO(updatedTitle, false, token))
        }
        val hm : HashMap<String, Any> = res.body()
        val result: Map<String, String> = hm["data"] as Map<String, String>
        assertNotNull(hm["data"])
        assertEquals(updatedTitle, result["title"])
    }
}