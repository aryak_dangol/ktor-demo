package com.example.services

import com.example.models.User
import com.example.models.UserDTO
import com.example.repositories.UserRepository
import com.example.security.JwtConfig
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import java.util.Objects

class UserService {

    val userRepository : UserRepository =  UserRepository()

    fun registerUser(user: UserDTO): HashMap<String, String> {
        val newUser: User? = userRepository.createUser(User(null, user.fullName, user.username, user.password))
        val map : HashMap<String, String> = HashMap()
        return if (newUser !=null){
            map["id"] = newUser.id.toString()
            map["name"] = newUser.fullName
            map["username"] = newUser.username
            map["error"] = false.toString()
            return map
        } else{
            map["error"] = true.toString()
            map["message"] = "Something went wrong"
            return map
        }
    }

    fun loginUser(user: UserDTO): HashMap<String, String> {
        val foundUser : User? = userRepository.getUserByUsernameAndPassword(user.username, user.password)
        val map: HashMap<String, String> = HashMap()
        return if (foundUser!=null){
            val jwtConfig : JwtConfig = JwtConfig()
            map["id"] = foundUser.id.toString()
            map["username"] = foundUser.username
            map["error"] = false.toString()
            map["token"] = jwtConfig.createJwtToken(foundUser.id.toString())
            return map
        } else{
            map["error"] = true.toString()
            map["message"] = "No User Found With the given credentials"
            return map
        }
    }

    fun authenticate(jwtPrincipal: JWTPrincipal?): HashMap<String, String>{
        val id = jwtPrincipal?.payload?.getClaim("id")?.asString()
        val map: HashMap<String, String> = HashMap()
        if (id !=null){
            map["id"] = id
            map["message"] = "Logged in Successfully"
            return map
        }
        else{
            map["id"] = ""
            map["message"] = "Authentication failed"
            return map
        }

    }

}