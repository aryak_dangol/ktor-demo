package com.example.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm

class JwtConfig {
    private val algorithm = Algorithm.HMAC256("secret")

    private val ISSUER = "user-service"
    private val AUDIENCE = "user-service"

    private val CLAIM = "id"


    fun createJwtToken(id: String): String{
       return JWT.create()
            .withAudience(AUDIENCE)
            .withIssuer(ISSUER)
            .withClaim(CLAIM, id)
            .sign(algorithm)
    }

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(ISSUER)
        .withAudience(AUDIENCE)
        .build()

}