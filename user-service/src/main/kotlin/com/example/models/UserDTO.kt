package com.example.models

import kotlinx.serialization.Serializable


data class UserDTO(
    val fullName: String,
    val username: String,
    val password: String,
)
