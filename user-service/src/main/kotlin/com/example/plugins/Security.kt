package com.example.plugins

import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import com.example.security.JwtConfig
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*

fun Application.configureSecurity() {

    val jwtConfig : JwtConfig = JwtConfig()

    authentication {
            jwt {
                verifier(jwtConfig.verifier)
                validate{
                    val claim = it.payload.getClaim("id").asString()
                    if(claim != null){
                        JWTPrincipal(it.payload)
                    }
                    else{
                        null
                    }
                }
                challenge{ _, _ ->
                    val message: String = "Authentication Failed!"
                    call.respond(HttpStatusCode.Unauthorized, mapOf("message" to message, "error" to "true"))
                }

            }
        }

}
