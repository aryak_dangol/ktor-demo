package com.example.repositories

import com.example.models.User
import org.litote.kmongo.*


class UserRepository {
    private val client = KMongo.createClient( "mongodb+srv://aryak:aryak123@cluster0.tewmglw.mongodb.net/?retryWrites=true&w=majority")
    private val db = client.getDatabase("ktor-demo")
    private val users = db.getCollection<User>()

    fun createUser(user: User): User?{
        users.insertOne(user)
        return user
    }

    fun getUserByUsernameAndPassword(username: String, password : String): User? {
        return users.findOne(and(User::username eq username, User::password eq password))
    }



}