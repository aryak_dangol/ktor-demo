package com.example.routes

import com.example.models.UserDTO
import com.example.services.UserService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.litote.kmongo.util.idValue

val userService: UserService = UserService()

fun Route.userRoutes(){
    route("/user"){
        post("/signup"){
            val body = call.receive<UserDTO>()
            val res = userService.registerUser(body)
            if(res["error"] == "false"){
                call.respond(HttpStatusCode.Created, res)
            }
            else{
                call.respond(HttpStatusCode.InternalServerError, "Something went wrong")
            }
        }

        post("/login"){
            val body = call.receive<UserDTO>()
            val res = userService.loginUser(body)
            if (res["error"] == "false"){
                call.respond(HttpStatusCode.OK, res)
            }
            else{
                call.respond(HttpStatusCode.NotFound, "")
            }
        }

        authenticate {
            get("/get-user"){
                val principal = call.principal<JWTPrincipal>()
               val res = userService.authenticate(principal)
                if(res["id"] != ""){
                   call.respond(HttpStatusCode.OK,res)
                }
                else{
                    call.respond(HttpStatusCode.Unauthorized, res)
                }
            }
        }
    }

}