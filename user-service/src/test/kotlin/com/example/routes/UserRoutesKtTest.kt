package com.example.routes

import io.ktor.http.*

import com.example.models.UserDTO
import com.example.plugins.configureRouting
import com.example.plugins.configureSecurity
import com.example.plugins.configureSerialization

import io.ktor.client.call.*

import io.ktor.client.plugins.contentnegotiation.*

import io.ktor.client.request.*

import io.ktor.serialization.kotlinx.json.*

import kotlin.test.*
import io.ktor.server.testing.*
import kotlinx.serialization.json.Json

internal class UserRoutesKtTest {

    @org.junit.jupiter.api.Test
    fun getUserService() {

    }

    @Test
    fun testSignUpRoute() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
                json(Json {
                    isLenient = true
                })
            }
        }
        val res = client.post("/user/signup") {
            contentType(ContentType.Application.Json)
            setBody(mapOf("fullName" to "User ABC", "username" to "userabc", "password" to "userabc"))
        }
        val hm: HashMap<String, String> = res.body()
        assertEquals(HttpStatusCode.Created, res.status)
        assertEquals(hm["username"], "userabc")
        assertEquals(hm["error"], "false")
    }

    @Test
    fun testLoginRoute() = testApplication {
        application {
            configureRouting()
            configureSerialization()
        }
        val client = createClient {
            install(ContentNegotiation) {
                json(Json {
                    isLenient = true
                })
            }
        }
        val res = client.post("/user/login") {
            contentType(ContentType.Application.Json)
            setBody(mapOf("username" to "johndoe", "password" to "john123"))
        }
        val hm: HashMap<String, String> = res.body()
        println(hm["token"])
        assertEquals(HttpStatusCode.OK, res.status)
        assertEquals(hm["username"], "johndoe")
        assertEquals(hm["error"], "false")
    }

    @Test
    fun testAuthenticationRoute() = testApplication {
        application {
            configureSerialization()
            configureRouting()
            configureSecurity()
        }

        val client = createClient {
            install(ContentNegotiation) {
                json(Json {
                    isLenient = true
                })
            }
        }

        val token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyLXNlcnZpY2UiLCJpc3MiOiJ1c2VyLXNlcnZpY2UiLCJpZCI6IjYyZjM1MjkzODg4NTY0NTA1NjcxZmEwOCJ9.BDxMe1GHOIyStNHpcpo0w_tIBzSIdsJg8GWbyJMiYiw"

        val res = client.get("/user/get-user") {
            headers {
                append("Authorization", "Bearer $token")
            }


        }
        val hm: HashMap<String, String> = res.body()
        assertEquals("62f35293888564505671fa08", hm["id"])
    }
}